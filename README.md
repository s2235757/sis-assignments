# SIS assignments: Data triplification
For this assignment I created a python script called Transformer.py that triplifies the data given at the argument path. 
Unfortunately, the code has an exponential time compelxity due to the nested for loops. I could not find any way around this and thus the code cannot transform the 
entire dataset due to memory issues. However, as you can see and test it works for a smaller subset of the data. Hopefully my code also shows my understanding
of data triplification and the usage of an ontology such as SAREF to create RDF data.
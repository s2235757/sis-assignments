from rdflib import Graph, Namespace, Literal, URIRef
from rdflib.namespace import RDF, XSD
import pandas as pd
import sys

if __name__ == '__main__':
    # Ensure that the script is run with the dataset path argument
    if len(sys.argv) != 2:
        print("Usage: python script.py <path_to_dataset>")
        sys.exit(1)

    path_to_dataset = sys.argv[1]

    # Define namespaces
    SAREF = Namespace("https://ontology.tno.nl/saref#")
    EXAMPLE = Namespace("http://example.org/")

    # Load dataset
    data = pd.read_csv(path_to_dataset, nrows=800)
    data.drop(columns=['cet_cest_timestamp'], inplace=True)

    # Create an RDF graph
    g = Graph()

    # Bind namespaces to prefixes for easier use
    g.bind("saref", SAREF)
    g.bind("ex", EXAMPLE)

    # Function to map dataset columns to RDF properties
    def map_to_rdf(column):
        # Split column name into building name and device name
        parts = column.split("_")
        building_name = "_".join(parts[:3])
        device_name = "_".join(parts[3:])
        return building_name, device_name

    # Preprocess column mappings to avoid repetitive operations inside the loop
    column_mappings = {column: map_to_rdf(column) for column in data.columns if column != 'utc_timestamp'}

    # Apply mapping to each column name in the dataset
    for index, row in data.iterrows():
        timestamp = row['utc_timestamp']
        for column, (building, device) in column_mappings.items():
            value = row[column]
            if pd.notna(value) and isinstance(value, (int, float)):
                measurement_uri = URIRef(f"http://example.org/resource/{building}_{device}_{timestamp}")
                building_uri = URIRef(f"http://example.org/resource/{building}")
                device_uri = URIRef(f"http://example.org/resource/{device}")
                property_uri = URIRef("http://example.org/resource/Property/" + building + "_" + device + "_" + timestamp)

                # Add triples to the graph
                g.add((measurement_uri, RDF.type, SAREF.Measurement))  # The Measurement instance is of type Measurement
                g.add((measurement_uri, SAREF.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))  # Add a timestamp to the measurement
                g.add((measurement_uri, SAREF.hasValue, Literal(value, datatype=XSD.float)))  # Add the value to the measurement
                g.add((measurement_uri, SAREF.isMeasuredIn, SAREF.KiloWattHour))  # Give a unit to the measurement
                # g.add((measurement_uri, EXAMPLE['isMeasuredBy'], device_uri))  # Link the measurement to a device

                g.add((device_uri, RDF.type, SAREF.Device))  # The Device instance is of type Device
                g.add((device_uri, SAREF.hasModel, Literal(device, datatype=XSD.string)))  # Add a model to the device
                g.add((device_uri, SAREF.isLocatedIn, building_uri))  # Link the device to the building
                # g.add((device_uri, EXAMPLE['hasMeasurement'], measurement_uri))  # The device makes measurements

                g.add((property_uri, RDF.type, SAREF.Property))  # A property instance is of type Property
                g.add((property_uri, SAREF.isMeasuredByDevice, device_uri))  # the property is measured by a device
                g.add((property_uri, SAREF.relatesToMeasurement, measurement_uri))  # the property relates to a measurement

                g.add((building_uri, RDF.type, SAREF.Building))  # The Building instance is of type Building
                g.add((building_uri, SAREF.contains, device_uri))  # The building contains devices

    # Serialize the RDF graph to Turtle format
    output_file = "graph.ttl"
    g.serialize(destination=output_file, format='turtle')

    print("RDF graph serialized and saved to", output_file)
